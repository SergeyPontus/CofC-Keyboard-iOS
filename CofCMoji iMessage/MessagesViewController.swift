//
//  MessagesViewController.swift
//  CofCMoji iMessage
//
//  Created by Evgeniy Tkachenko on 15.02.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import UIKit
import Messages
import RealmSwift
import Kingfisher
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import ImageIO
import ObjectMapper

class MessagesViewController: MSMessagesAppViewController, MSStickerBrowserViewDataSource {
    
    private let Width = UIScreen.main.bounds.size.width
    private let Height = UIScreen.main.bounds.size.height
    
    var vwCategory: UIView!
    let controller = MSStickerBrowserViewController(stickerSize: .small)
    
    var stickers = [MSSticker]()
    
    var firstOpen: Bool!
    var btLayoutConstraint: NSLayoutConstraint!
    
    var selectedCategories: Int = 0 {
        didSet {
            self.stickers.removeAll()
            self.loadStickers()
            self.controller.stickerBrowserView.reloadData()
        }
    }
    var countCategories: Int = 0
    
    var stickersOfCategory: [Int: [Int]] = [:]
    var arrayButtonsByCategory: [UIButton] = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let standartDefaults: UserDefaults = UserDefaults()
        if standartDefaults.string(forKey: "firstOpen") != "true" {
            standartDefaults.set("true", forKey: "firstOpen")
            standartDefaults.synchronize()
            self.saveStaticSticker()
        }
        
        let realm = try! Realm()
        let times = realm.objects(LastUpdate.self)
        if times.count == 0 {
            let update = LastUpdate()
            try! realm.write {
                realm.add(update, update: true)
            }
        }
        
        self.firstOpen = true
        
        var desiredHeight:CGFloat!
        desiredHeight = 216
        
        if (self.btLayoutConstraint != nil){
            view.removeConstraint(self.btLayoutConstraint)
            self.btLayoutConstraint = nil
        }
        self.btLayoutConstraint = NSLayoutConstraint(item: view,
                                                     attribute: .height,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: desiredHeight)
        self.btLayoutConstraint.priority = UILayoutPriority(UILayoutPriorityRequired)
        
        self.view.addConstraint(self.btLayoutConstraint)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.createStickerBrowser()
        
        self.saveInfo()
        self.addButtonsByCategory()
    }
    
    func createStickerBrowser() {
        self.controller.view.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.view.frame.size.width, height: self.view.frame.size.height - 85)
        self.controller.view.backgroundColor = UIColor.lightGray
        
        addChildViewController(self.controller)
        self.view.addSubview(self.controller.view)
        
        self.vwCategory = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height - 85, width: Width, height: 42))
        self.vwCategory.backgroundColor = UIColor(red:0.73, green:0.71, blue:0.64, alpha:1.0)
        self.view.addSubview(self.vwCategory)
        
        
        self.controller.stickerBrowserView.dataSource = self
    }
    
    func loadStickers() {
        
        var number = 0
        if self.selectedCategories == 100000 {
            if self.getLastStickersCount() <= 12 {
                number = self.getLastStickersCount()
            } else {
                number = 12
            }
        } else {
            if let stickers = self.stickersOfCategory[self.selectedCategories] {
                number = stickers.count
            } else {
                number = 0
            }
        }

        if self.selectedCategories == 100000 {
            for newNumber in 0..<number {
                let realm = try! Realm()
                if let lastSticker = realm.objects(LastSticker.self).filter("number = \(newNumber)").first {
                    if let url = Bundle.main.url(forResource: "sticker\(lastSticker.id)", withExtension: "gif") {
                        do {
                            let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                            self.stickers.append(sticker)
                        } catch {
                            print(error)
                        }
                    } else if lastSticker.type == "gif" {
                        if let url = self.getImage(name: "sticker\(lastSticker.id).gif") {
                            do {
                                let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                self.stickers.append(sticker)
                            } catch {
                                print(error)
                            }
                        }
                    } else {
                        if let url = Bundle.main.url(forResource: "sticker\(lastSticker.id)", withExtension: "png") {
                            do {
                                let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                self.stickers.append(sticker)
                            } catch {
                                print(error)
                            }
                        } else if let url = self.getImage(name: "sticker\(lastSticker.id).png") {
                            do {
                                let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                self.stickers.append(sticker)
                            } catch {
                                print(error)
                            }
                        }
                    }
                }
            }
        } else {
            if let stickers = self.stickersOfCategory[self.selectedCategories] {
                for newNumber in 0..<number {
                    let realm = try! Realm()
                    if let lastSticker = realm.objects(Sticker.self).filter("id = \(stickers[newNumber])").first {
                        
                        if let url = Bundle.main.url(forResource: "sticker\(lastSticker.id)", withExtension: "gif") {
                            do {
                                let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                self.stickers.append(sticker)
                            } catch {
                                print(error)
                            }
                        } else if lastSticker.type == "gif" {
                            if let url = self.getImage(name: "sticker\(lastSticker.id).gif") {
                                do {
                                    let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                    self.stickers.append(sticker)
                                } catch {
                                    print(error)
                                }
                            }
                        } else {
                            if let url = self.getImage(name: "sticker\(lastSticker.id).png") {
                                do {
                                    let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                    self.stickers.append(sticker)
                                } catch {
                                    print(error)
                                }
                            } else if let urlpath = Bundle.main.path(forResource: "sticker\(lastSticker.id)", ofType: "png") {
                                let urll = NSURL.fileURL(withPath: urlpath)
                                do {
                                    let sticker = try MSSticker(contentsOfFileURL: urll, localizedDescription: "")
                                    self.stickers.append(sticker)
                                } catch {
                                    print(error)
                                }
                            }
                        }
                        
                        /*
                        if let url = Bundle.main.url(forResource: "sticker\(lastSticker.id)", withExtension: "gif") {
                            do {
                                let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                self.stickers.append(sticker)
                            } catch {
                                print(error)
                            }
                        } else if lastSticker.type == "gif" {
                            if let url = self.getImage(name: "sticker\(lastSticker.id).gif") {
                                do {
                                    let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                    self.stickers.append(sticker)
                                } catch {
                                    print(error)
                                }
                            }
                        } else {
                            if let size = self.getSizeImage(name: "sticker\(lastSticker.id).png") {
                                var processor: ResizingImageProcessor
                                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                                    processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 6 * 30), height: (size.height * (Width - 6 * 30)) / size.width))
                                } else {
                                    processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 4 * 20)/CGFloat(scale), height: (size.height * (Width - 4 * 20)/CGFloat(scale)) / size.width))
                                }
                                
                                if let urlpath = Bundle.main.path(forResource: "sticker\(lastSticker.id)", ofType: "png") {
                                    let urll = NSURL.fileURL(withPath: urlpath)
                                    do {
                                        let sticker = try MSSticker(contentsOfFileURL: urll, localizedDescription: "")
                                        self.stickers.append(sticker)
                                    } catch {
                                        print(error)
                                    }
                                }
                                
                                if let url = self.getImage(name: "sticker\(lastSticker.id).png") {
                                    do {
                                        let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                        self.stickers.append(sticker)
                                    } catch {
                                        print(error)
                                    }
                                }
                            }
                            
                            
                            
                            /*
                            if let url = Bundle.main.url(forResource: "sticker\(lastSticker.id)", withExtension: "png") {
                                do {
                                    let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                    self.stickers.append(sticker)
                                } catch {
                                    print(error)
                                }
                            } else if let url = self.getImage(name: "sticker\(lastSticker.id).png") {
                                do {
                                    let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                                    self.stickers.append(sticker)
                                } catch {
                                    print(error)
                                }
                            }
                            */
                        }
                        */
                    }
                }
            }
        }
    }
    
    func loadStickers2() {
        for i in 6...14 {
            if let url = Bundle.main.url(forResource: "sticker\(i)", withExtension: "png") {
                do {
                    let sticker = try MSSticker(contentsOfFileURL: url, localizedDescription: "")
                    stickers.append(sticker)
                } catch {
                    print(error)
                }
            }
        }
        self.controller.stickerBrowserView.reloadData()
    }
    
    func saveInfo() {
        let realm = try! Realm()
        if let times = realm.objects(LastUpdate.self).first {
            Alamofire.request("https://api.classatcofc.com/stickersimsg/time?time=\(times.time)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
                
                if let error = response.result.error {
                    print(error.localizedDescription)
                    self.getStickers()
                    self.saveCategories()
                    self.saveStickers()
                } else if let timeLastUpdateResponse = response.result.value {
                    
                    let realm = try! Realm()
                    let update = LastUpdate()
                    update.time = Int(Date().timeIntervalSince1970)
                    try! realm.write {
                        realm.add(update, update: true)
                    }
                    
                    let categories = timeLastUpdateResponse.categories
                    let stickers = timeLastUpdateResponse.stickers
                    for category in categories {
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(category, update: true)
                        }
                    }
                    for sticker in stickers {
                        let type = sticker.iconUrl.components(separatedBy: ".").last!
                        sticker.type = type
                        
                        if type == "png" {
                            let name = "sticker\(sticker.id).png"
                            let fileManager = FileManager.default
                            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                            
                            do {
                                try fileManager.removeItem(atPath: paths)
                            } catch let error as NSError {
                                print(error.debugDescription)
                            }
                        } else {
                            var name = "sticker\(sticker.id).gif"
                            let fileManager = FileManager.default
                            var paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                            
                            do {
                                try fileManager.removeItem(atPath: paths)
                            } catch let error as NSError {
                                print(error.debugDescription)
                            }
                            
                            name = "stickerThumb\(sticker.id).gif"
                            paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                            
                            do {
                                try fileManager.removeItem(atPath: paths)
                            } catch let error as NSError {
                                print(error.debugDescription)
                            }
                        }
                        
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(sticker, update: true)
                        }
                    }
                    self.getStickers()
                    self.saveCategories()
                    self.saveStickers()
                }
            }
        }
        
        
        /*
        ServerKeyboard.sharedInstance().getAllCategoriesAndStickers(completionBlock: { (success, error, categories, stickers) in
            if success {
                for category in categories {
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(category, update: true)
                    }
                }
                for sticker in stickers {
                    let type = sticker.iconUrl.components(separatedBy: ".").last!
                    sticker.type = type
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(sticker, update: true)
                    }
                }
                self.getStickers()
                self.saveCategories()
                self.saveStickers()
            } else {
                self.getStickers()
                self.saveCategories()
                self.saveStickers()
            }
        })
        */
    }
    
    func saveCategories() {
        let realm = try! Realm()
        let categories = realm.objects(Category.self)
        
        for category in categories {
            if !category.isHide {
                if let url = URL(string: "https://api.classatcofc.com/" + category.iconUrl) {
                    self.saveImageDocumentDirectory(url: url, name: "category\(category.id).png", completion: { (success) in

                    })
                }
            }
        }
        self.perform(#selector(self.showBtLastStickers), with: self, afterDelay: 1)
    }
    
    func saveStickers() {
        let realm = try! Realm()
        let stickers = realm.objects(Sticker.self)
        for sticker in stickers {
            if !sticker.isHide {
                let type = sticker.iconUrl.components(separatedBy: ".").last!
                if type != "gif" {
                    if let url = URL(string: "https://api.classatcofc.com/" + sticker.iconUrl) {
                        self.saveImageDocumentDirectory(url: url, name: "sticker\(sticker.id).png", completion: { (success) in
                            if success {
                            }
                        })
                    }
                } else {
                    if let url = URL(string: "https://api.classatcofc.com/" + sticker.iconThumbUrl) {
                        self.saveImageDocumentDirectory(url: url, name: "stickerThumb\(sticker.id).gif", completion: { (success) in
                            if success {
                                
                            }
                        })
                    }
                    if let url = URL(string: "https://api.classatcofc.com/" + sticker.iconUrl) {
                        self.saveImageDocumentDirectory(url: url, name: "sticker\(sticker.id).gif", completion: { (success) in
                            if success {
                                
                            }
                        })
                    }
                }
            }
        }
    }
    
    func getStickers() {
        let realm = try! Realm()
        let stickers = realm.objects(Sticker.self)
        
        let stickersSort = stickers.sorted {
            $0.serialNumber < $1.serialNumber
        }
        
        self.stickersOfCategory = [:]
        
        for sticker in stickersSort {
            if !sticker.isHide {
                if self.stickersOfCategory[sticker.categoryId] != nil {
                    self.stickersOfCategory[sticker.categoryId]!.append(sticker.id)
                } else {
                    self.stickersOfCategory[sticker.categoryId] = [sticker.id]
                }
            }
        }
        print(self.stickersOfCategory)
    }
    
    func saveStaticSticker() {
        
        if let urlpath = Bundle.main.path(forResource: "StickersJSON", ofType: "json") {
            let url = NSURL.fileURL(withPath: urlpath)
            
            Alamofire.request(url.absoluteString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
                
                if let error = response.result.error {
                    print(error.localizedDescription)
                } else if let timeLastUpdateResponse = response.result.value {
                    let categories = timeLastUpdateResponse.categories
                    let stickers = timeLastUpdateResponse.stickers
                    for category in categories {
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(category, update: true)
                        }
                    }
                    for sticker in stickers {
                        let type = sticker.iconUrl.components(separatedBy: ".").last!
                        sticker.type = type
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(sticker, update: true)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - UIImage
    
    func saveImageDocumentDirectory(url: URL, name: String, completion: @escaping (_ success: Bool) -> ()) {
        self.getDataFromUrl(url: url) { (data, URLResponse, error) in
            if let data = data {
                print("download \(name)")
                let fileManager = FileManager.default
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                
                fileManager.createFile(atPath: paths as String, contents: data, attributes: nil)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getImage(name: String) -> URL? {
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(name)
        let url = URL.init(fileURLWithPath: imagePAth)
        return url
    }
    
    func getSizeImage(name: String) -> CGSize? {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(name)
        if fileManager.fileExists(atPath: imagePAth) {
            let image = UIImage(contentsOfFile: imagePAth)
            return image?.size
        } else {
            print("No Image")
            return nil
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> ()) {
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func clearAllImage() {
        let fileManager = FileManager.default
        let documentsUrl = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
        let documentsPath = documentsUrl.path
        
        do {
            if let documentPath = documentsPath {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache: \(fileNames)")
                for fileName in fileNames {
                    
                    if (fileName.hasSuffix(".png")) {
                        let filePathName = "\(documentPath)/\(fileName)"
                        try fileManager.removeItem(atPath: filePathName)
                    }
                }
                
                let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache after deleting images: \(files)")
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }

    // MARK: - Buttons by Category
    
    func addButtonsByCategory() {
        if self.getLastStickersCount() == 0 {
            
            let realm = try! Realm()
            let categories = realm.objects(Category.self)
            
            let categoriesSort = categories.sorted {
                $0.sort < $1.sort
            }
            
            var number = 0
            for category in categoriesSort {
                if !category.isHide {
                    number += 1
                }
            }
            
            var i = 0
            for category in categoriesSort {
                if !category.isHide {
                    let button = UIButton()
                    button.frame = CGRect(x: Width * CGFloat(i) / CGFloat(number), y: 0, width: Width / CGFloat(number), height: 42)
                    
                    if let size = self.getSizeImage(name: "category\(category.id).png") {
                        let processor = ResizingImageProcessor(targetSize: CGSize(width: size.width, height: size.height))
                        
                        if let url = self.getImage(name: "category\(category.id).png") {
                            do {
                                _ = try Data(contentsOf: url)
                                button.kf.setImage(with: url, for: .normal, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                                button.kf.setImage(with: url, for: .selected, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    } else {
                        button.setImage(UIImage(named: "category\(category.id)"), for: .normal)
                        button.setImage(UIImage(named: "category\(category.id)"), for: .selected)
                    }
                    button.tag = category.id
                    button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
                    button.imageView?.contentMode = .scaleAspectFit
                    button.addTarget(self, action: #selector(self.tapCategory(sender:)), for: .touchUpInside)
                    self.arrayButtonsByCategory.append(button)
                    self.vwCategory.addSubview(button)
                    
                    if i == self.selectedCategories {
                        button.backgroundColor = UIColor(red: 0.31, green: 0.11, blue: 0.07, alpha: 1.0)
                    }
                    
                    i += 1
                }
            }
        } else {
            let realm = try! Realm()
            let categories = realm.objects(Category.self)
            
            let categoriesSort = categories.sorted {
                $0.sort < $1.sort
            }
            
            var number = 0
            for category in categoriesSort {
                if !category.isHide {
                    number += 1
                }
            }
            number+=1
            
            let button = UIButton()
            button.frame = CGRect(x: 0, y: 0, width: Width / CGFloat(number), height: 42)
            button.tag = 100000
            button.setImage(UIImage(named: "Bt-Recently"), for: .normal)
            button.setImage(UIImage(named: "Bt-Recently"), for: .selected)
            button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
            button.imageView?.contentMode = .scaleAspectFit
            button.addTarget(self, action: #selector(self.tapCategory(sender:)), for: .touchUpInside)
            self.arrayButtonsByCategory.append(button)
            
            var i = 1
            for category in categoriesSort {
                if !category.isHide {
                    let button = UIButton()
                    
                    button.frame = CGRect(x: Width * CGFloat(i) / CGFloat(number), y: 0, width: Width / CGFloat(number), height: 42)
                    button.tag = category.id
                    
                    if let size = self.getSizeImage(name: "category\(category.id).png") {
                        let processor = ResizingImageProcessor(targetSize: CGSize(width: size.width, height: size.height))
                        
                        
                        if let url = self.getImage(name: "category\(category.id).png") {
                            do {
                                let data = try Data(contentsOf: url)
                                button.kf.setImage(with: url, for: .normal, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                                button.kf.setImage(with: url, for: .selected, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    } else {
                        button.setImage(UIImage(named: "category\(category.id)"), for: .normal)
                        button.setImage(UIImage(named: "category\(category.id)"), for: .selected)
                    }
                    
                    button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
                    button.imageView?.contentMode = .scaleAspectFit
                    button.addTarget(self, action: #selector(self.tapCategory(sender:)), for: .touchUpInside)
                    self.arrayButtonsByCategory.append(button)
                    self.vwCategory.addSubview(button)
                    
                    if i == self.selectedCategories {
                        button.backgroundColor = UIColor(red: 0.31, green: 0.11, blue: 0.07, alpha: 1.0)
                    }
                    
                    i += 1
                }
            }
        }

        if firstOpen == true {
            firstOpen = false
            if let bt = self.arrayButtonsByCategory.first {
                self.tapCategory(sender: bt)
            }
        } else {
            for bt in self.arrayButtonsByCategory {
                if bt.tag == self.selectedCategories {
                    self.tapCategory(sender: bt)
                }
            }
        }
        
    }
    
    func showBtLastStickers() {
        if arrayButtonsByCategory.count != 0 {
            for button in arrayButtonsByCategory {
                button.removeFromSuperview()
            }
            self.arrayButtonsByCategory.removeAll()
        }
        self.addButtonsByCategory()
    }
    
    func setLastSticker(stickerId: Int, stickerType: String) {
        let realm = try! Realm()
        
        let lastSticker = realm.objects(LastSticker.self).filter("id = \(stickerId)").first
        
        if let lastSticker = lastSticker {
            try! realm.write {
                realm.delete(lastSticker)
            }
            
            let listLastStickers = realm.objects(LastSticker.self)
            let listLastStickersSort = listLastStickers.sorted {
                $0.number < $1.number
            }
            
            var i = 1
            for lastSticker in listLastStickersSort {
                try! realm.write {
                    lastSticker.number = i
                }
                i += 1
            }
            
            let lastSticker = LastSticker()
            lastSticker.number = 0
            lastSticker.id = stickerId
            lastSticker.type = stickerType
            try! realm.write {
                realm.add(lastSticker)
            }
        } else {
            let listLastStickers = realm.objects(LastSticker.self)
            for lastSticker in listLastStickers {
                try! realm.write {
                    lastSticker.number += 1
                }
            }
            
            let lastSticker = LastSticker()
            lastSticker.number = 0
            lastSticker.id = stickerId
            lastSticker.type = stickerType
            try! realm.write {
                realm.add(lastSticker)
            }
        }
        
        if self.getLastStickersCount() == 1 {
            self.showBtLastStickers()
        }
        
        if self.selectedCategories == 100000 {
            self.controller.stickerBrowserView.reloadData()
        }
    }
    
    func getLastStickersCount() -> Int {
        let realm = try! Realm()
        let listLastStickers = realm.objects(LastSticker.self)
        print(listLastStickers)
        return listLastStickers.count
    }
    
    // MARK: - Actions
    
    func tapCategory(sender: UIButton){
        self.selectedCategories = sender.tag
        for bt in self.arrayButtonsByCategory {
            if bt == sender {
                bt.backgroundColor = UIColor(red: 0.31, green: 0.11, blue: 0.07, alpha: 1.0)
            } else {
                bt.backgroundColor = UIColor.clear
            }
        }
    }
    
    // MARK: - MSStickerBrowserView DataSource
    
    func numberOfStickers(in stickerBrowserView: MSStickerBrowserView) -> Int {
        return stickers.count
    }
    
    func stickerBrowserView(_ stickerBrowserView: MSStickerBrowserView, stickerAt index: Int) -> MSSticker {
        return stickers[index]
    }
    
    // MARK: - Conversation Handling
    
    override func willBecomeActive(with conversation: MSConversation) {
        // Called when the extension is about to move from the inactive to active state.
        // This will happen when the extension is about to present UI.
        
        // Use this method to configure the extension and restore previously stored state.
    }
    
    override func didResignActive(with conversation: MSConversation) {
        // Called when the extension is about to move from the active to inactive state.
        // This will happen when the user dissmises the extension, changes to a different
        // conversation or quits Messages.
        
        // Use this method to release shared resources, save user data, invalidate timers,
        // and store enough state information to restore your extension to its current state
        // in case it is terminated later.
    }
   
    override func didReceive(_ message: MSMessage, conversation: MSConversation) {
        // Called when a message arrives that was generated by another instance of this
        // extension on a remote device.
        
        // Use this method to trigger UI updates in response to the message.
    }
    
    override func didStartSending(_ message: MSMessage, conversation: MSConversation) {
        // Called when the user taps the send button.
    }
    
    override func didCancelSending(_ message: MSMessage, conversation: MSConversation) {
        // Called when the user deletes the message without sending it.
    
        // Use this to clean up state related to the deleted message.
    }
    
    override func willTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        UIView.animate(withDuration: 0.25) {
            self.vwCategory.alpha = 0
        }
    }
    
    override func didTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            if presentationStyle == .compact {
                self.vwCategory.frame.origin.y = self.view.frame.size.height - 85
                self.controller.view.frame = CGRect(x: CGFloat(0), y: CGFloat(-80), width: self.view.frame.size.width, height: self.view.frame.size.height)
            } else {
                self.vwCategory.frame.origin.y = self.view.frame.size.height - 85
                self.controller.view.frame = CGRect(x: CGFloat(0), y: CGFloat(-20), width: self.view.frame.size.width, height: self.view.frame.size.height - 65)
            }
            UIView.animate(withDuration: 0.25) {
                self.vwCategory.alpha = 1
            }
        } else {
            if presentationStyle == .compact {
                self.vwCategory.frame.origin.y = self.view.frame.size.height - 85
                self.controller.view.frame = CGRect(x: CGFloat(0), y: CGFloat(-80), width: self.view.frame.size.width, height: self.view.frame.size.height)
            } else {
                self.vwCategory.frame.origin.y = self.view.frame.size.height - 85
                self.controller.view.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.view.frame.size.width, height: self.view.frame.size.height - 65)
            }
            UIView.animate(withDuration: 0.25) {
                self.vwCategory.alpha = 1
            }
        }
    }
}
