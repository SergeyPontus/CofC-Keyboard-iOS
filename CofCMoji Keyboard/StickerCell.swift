//
//  StickerCell.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 22.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import UIKit
import Kingfisher

class StickerCell: UICollectionViewCell {
    
    @IBOutlet weak var imSticker: AnimatedImageView!
    
    var stickerId = 0
    var stickerType = "png"
    
}
