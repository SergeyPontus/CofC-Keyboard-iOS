//
//  MessageView.swift
//  CofCMoji
//
//  Created by Evgeniy Tkachenko on 10.01.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import UIKit

class MessageView: UIVisualEffectView {
    
    private let Width = UIScreen.main.bounds.size.width
    var screen: UIViewController!
    var notHide: Bool!
    
    var view: UIView!
    
    init(screen: UIViewController) {
        super.init(effect: UIBlurEffect(style: .light))
        
        self.screen = screen
        
        self.frame = CGRect(x: (Width - 250) / 2, y: 80 - 30, width: 250, height: 80)
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.isOpaque = false
        
        let lbMessage = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        lbMessage.text = "This moji is copied to clipboard!\nLongpress in entry field and choose “Paste”"
        lbMessage.textAlignment = .center
        lbMessage.numberOfLines = 0
        self.addSubview(lbMessage)
    }
    
    init(screen: UIViewController, message: String) {
        super.init(effect: UIBlurEffect(style: .light))
        
        self.screen = screen
        
        self.frame = CGRect(x: (Width - 250) / 2, y: 80 - 30, width: 250, height: 80)
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.isOpaque = false
        
        let lbMessage = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        lbMessage.text = message
        lbMessage.textAlignment = .center
        lbMessage.numberOfLines = 0
        self.addSubview(lbMessage)
    }
    
    init(screen: UIViewController, message: String, notHide: Bool) {
        super.init(effect: UIBlurEffect(style: .light))
        
        self.screen = screen
        self.notHide = notHide
        
        self.frame = CGRect(x: (Width - 250) / 2, y: 80 - 30, width: 250, height: 80)
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        let lbMessage = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        lbMessage.text = message
        lbMessage.textAlignment = .center
        lbMessage.numberOfLines = 0
        self.addSubview(lbMessage)
    }
    
    func show() {
        self.view = UIView(frame: screen.view.frame)
        self.view.frame.size.height -= 70
        self.screen.view.addSubview(self.view)
        
        self.screen.view.addSubview(self)
        self.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 1
        }) { (success) in
            if success {
                if self.notHide == nil {
                    self.perform(#selector(self.hidden), with: nil, afterDelay: 3)
                }
            }
        }
    }
    
    func hidden() {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0
        }) { (success) in
            if success {
                self.view.removeFromSuperview()
                self.removeFromSuperview()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
