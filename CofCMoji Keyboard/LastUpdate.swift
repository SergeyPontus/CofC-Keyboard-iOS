//
//  LastUpdate.swift
//  CofCMoji
//
//  Created by Evgeniy Tkachenko on 19.04.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class LastUpdate: Object {
    dynamic var id = 0
    dynamic var time = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
