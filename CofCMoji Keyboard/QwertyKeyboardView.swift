//
//  QwertyKeyboardView.swift
//  Divine 9
//
//  Created by Evgeniy Tkachenko on 12.04.17.
//  Copyright © 2017 BigDig. All rights reserved.
//

import UIKit

class QwertyKeyboardView: UIView {
    
    @IBOutlet weak var btQ: UIButton!
    @IBOutlet weak var btW: UIButton!
    @IBOutlet weak var btE: UIButton!
    @IBOutlet weak var btR: UIButton!
    @IBOutlet weak var btT: UIButton!
    @IBOutlet weak var btY: UIButton!
    @IBOutlet weak var btU: UIButton!
    @IBOutlet weak var btI: UIButton!
    @IBOutlet weak var btO: UIButton!
    @IBOutlet weak var btP: UIButton!
    
    @IBOutlet weak var btA: UIButton!
    @IBOutlet weak var btS: UIButton!
    @IBOutlet weak var btD: UIButton!
    @IBOutlet weak var btF: UIButton!
    @IBOutlet weak var btG: UIButton!
    @IBOutlet weak var btH: UIButton!
    @IBOutlet weak var btJ: UIButton!
    @IBOutlet weak var btK: UIButton!
    @IBOutlet weak var btL: UIButton!
    
    @IBOutlet weak var btZ: UIButton!
    @IBOutlet weak var btX: UIButton!
    @IBOutlet weak var btC: UIButton!
    @IBOutlet weak var btV: UIButton!
    @IBOutlet weak var btB: UIButton!
    @IBOutlet weak var btN: UIButton!
    @IBOutlet weak var btM: UIButton!
    
    @IBOutlet weak var btShift: UIButton!
    @IBOutlet weak var btBackspace: UIButton!
    @IBOutlet weak var btNumbers: UIButton!
    @IBOutlet weak var btNextKeyboard: UIButton!
    @IBOutlet weak var btStickers: UIButton!
    @IBOutlet weak var btSpace: UIButton!
    @IBOutlet weak var btReturn: UIButton!
    
    var isShift: Bool = false {
        didSet {
            self.changeTypeKeyboard()
        }
    }
    
    var isNumber: Bool = false {
        didSet {
            self.changeTypeKeyboard()
        }
    }
    
    // MARK: - Init functions

    class func instanceFromNib() -> QwertyKeyboardView {
        return UINib(nibName: "QwertyKeyboardView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! QwertyKeyboardView
    }
    
    // MARK: - Custom Buttons
    
    func customButtons() {
        for view in self.subviews {
            if view is UIButton {
                view.layer.shadowColor = UIColor.black.cgColor
                view.layer.shadowOpacity = 0.4
                view.layer.shadowRadius = 1
                view.layer.shadowOffset = CGSize(width: 0, height: 1)
            }
        }
    }
    
    // MARK: - Type Keyboard
    
    func changeTypeKeyboard() {
        if self.isNumber {
            self.btNumbers.setTitle("ABC", for: .normal)
            self.btShift.setImage(nil, for: .normal)
            
            if self.isShift {
                self.btShift.setTitle("123", for: .normal)
                
                self.btQ.setTitle("[", for: .normal)
                self.btW.setTitle("]", for: .normal)
                self.btE.setTitle("{", for: .normal)
                self.btR.setTitle("}", for: .normal)
                self.btT.setTitle("#", for: .normal)
                self.btY.setTitle("%", for: .normal)
                self.btU.setTitle("^", for: .normal)
                self.btI.setTitle("*", for: .normal)
                self.btO.setTitle("+", for: .normal)
                self.btP.setTitle("=", for: .normal)
                
                self.btA.setTitle("_", for: .normal)
                self.btS.setTitle("\\", for: .normal)
                self.btD.setTitle("|", for: .normal)
                self.btF.setTitle("~", for: .normal)
                self.btG.setTitle("<", for: .normal)
                self.btH.setTitle(">", for: .normal)
                self.btJ.setTitle("$", for: .normal)
                self.btK.setTitle("±", for: .normal)
                self.btL.setTitle("§", for: .normal)
                
                self.btZ.setTitle(".", for: .normal)
                self.btX.setTitle(",", for: .normal)
                self.btC.setTitle("?", for: .normal)
                self.btV.setTitle("!", for: .normal)
                self.btB.setTitle("'", for: .normal)
                self.btN.setTitle("%", for: .normal)
                self.btM.setTitle("`", for: .normal)
            } else {
                self.btShift.setTitle("#+=", for: .normal)
                
                self.btQ.setTitle("1", for: .normal)
                self.btW.setTitle("2", for: .normal)
                self.btE.setTitle("3", for: .normal)
                self.btR.setTitle("4", for: .normal)
                self.btT.setTitle("5", for: .normal)
                self.btY.setTitle("6", for: .normal)
                self.btU.setTitle("7", for: .normal)
                self.btI.setTitle("8", for: .normal)
                self.btO.setTitle("9", for: .normal)
                self.btP.setTitle("0", for: .normal)
                
                self.btA.setTitle("-", for: .normal)
                self.btS.setTitle("/", for: .normal)
                self.btD.setTitle(":", for: .normal)
                self.btF.setTitle(";", for: .normal)
                self.btG.setTitle("(", for: .normal)
                self.btH.setTitle(")", for: .normal)
                self.btJ.setTitle("$", for: .normal)
                self.btK.setTitle("@", for: .normal)
                self.btL.setTitle("\"", for: .normal)
                
                self.btZ.setTitle(".", for: .normal)
                self.btX.setTitle(",", for: .normal)
                self.btC.setTitle("?", for: .normal)
                self.btV.setTitle("!", for: .normal)
                self.btB.setTitle("'", for: .normal)
                self.btN.setTitle("%", for: .normal)
                self.btM.setTitle("`", for: .normal)
            }
        } else {
            self.btNumbers.setTitle("123", for: .normal)
            self.btShift.setTitle(nil, for: .normal)
            
            if self.isShift {
                self.btShift.setImage(UIImage(named: "Bt-Shift-Sel"), for: .normal)
                
                self.btQ.setTitle("Q", for: .normal)
                self.btW.setTitle("W", for: .normal)
                self.btE.setTitle("E", for: .normal)
                self.btR.setTitle("R", for: .normal)
                self.btT.setTitle("T", for: .normal)
                self.btY.setTitle("Y", for: .normal)
                self.btU.setTitle("U", for: .normal)
                self.btI.setTitle("I", for: .normal)
                self.btO.setTitle("O", for: .normal)
                self.btP.setTitle("P", for: .normal)
                
                self.btA.setTitle("A", for: .normal)
                self.btS.setTitle("S", for: .normal)
                self.btD.setTitle("D", for: .normal)
                self.btF.setTitle("F", for: .normal)
                self.btG.setTitle("G", for: .normal)
                self.btH.setTitle("G", for: .normal)
                self.btJ.setTitle("H", for: .normal)
                self.btK.setTitle("J", for: .normal)
                self.btL.setTitle("K", for: .normal)
                
                self.btZ.setTitle("Z", for: .normal)
                self.btX.setTitle("X", for: .normal)
                self.btC.setTitle("C", for: .normal)
                self.btV.setTitle("V", for: .normal)
                self.btB.setTitle("B", for: .normal)
                self.btN.setTitle("N", for: .normal)
                self.btM.setTitle("M", for: .normal)
            } else {
                self.btShift.setImage(UIImage(named: "Bt-Shift"), for: .normal)
                
                self.btQ.setTitle("q", for: .normal)
                self.btW.setTitle("w", for: .normal)
                self.btE.setTitle("e", for: .normal)
                self.btR.setTitle("r", for: .normal)
                self.btT.setTitle("t", for: .normal)
                self.btY.setTitle("y", for: .normal)
                self.btU.setTitle("u", for: .normal)
                self.btI.setTitle("i", for: .normal)
                self.btO.setTitle("o", for: .normal)
                self.btP.setTitle("p", for: .normal)
                
                self.btA.setTitle("a", for: .normal)
                self.btS.setTitle("s", for: .normal)
                self.btD.setTitle("d", for: .normal)
                self.btF.setTitle("f", for: .normal)
                self.btG.setTitle("g", for: .normal)
                self.btH.setTitle("h", for: .normal)
                self.btJ.setTitle("j", for: .normal)
                self.btK.setTitle("k", for: .normal)
                self.btL.setTitle("l", for: .normal)
                
                self.btZ.setTitle("z", for: .normal)
                self.btX.setTitle("x", for: .normal)
                self.btC.setTitle("c", for: .normal)
                self.btV.setTitle("v", for: .normal)
                self.btB.setTitle("b", for: .normal)
                self.btN.setTitle("n", for: .normal)
                self.btM.setTitle("m", for: .normal)
            }
        }
    }
}
