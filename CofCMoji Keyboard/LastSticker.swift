//
//  LastSticker.swift
//  CofCMoji
//
//  Created by Evgeniy Tkachenko on 16.01.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class LastSticker: Object {
    dynamic var id = 0
    dynamic var number = 0
    dynamic var type = "png"
}

