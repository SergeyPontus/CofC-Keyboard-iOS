//
//  ServerMaps.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 30.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import SwiftyJSON

class TimeLastUpdateResponse: Mappable {
    var time: Int = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        time <- map["generalTimestamp"]
    }
}

class timeLastUpdate: Mappable {
    var time: Int = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        time <- map["generalTimestamp"]
    }
}

