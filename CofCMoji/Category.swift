//
//  Category.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 30.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

class Category: Object, Mappable {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var timestamp = 0
    dynamic var isHide = true
    dynamic var iconUrl = ""
    dynamic var sort = 0
    dynamic var sortStr = ""
    dynamic var iconData: Data?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        timestamp <- map["timestamp"]
        isHide <- map["is_hide"]
        iconUrl <- map["icon"]
        sortStr <- map["sort"]
        if let sort = Int(sortStr) {
            self.sort = sort
        }
    }
    
    func downloadImage(completion: @escaping (_ success: Bool) -> ()) {
        if let url = URL(string: "https://api.classatcofc.com/" + self.iconUrl) {
            getDataFromUrl(url: url) { (data, response, error)  in
                guard let data = data, error == nil else { return }
                self.iconData = data
                completion(true)
            }
        } else {
            print("Error")
            completion(false)
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> ()) {
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            completion(data, response, error)
        }.resume()
    }
}
