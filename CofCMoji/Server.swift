//
//  Server.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 30.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift
import AlamofireObjectMapper
import ImageIO
import Kingfisher

class NewServer: NSObject {
    
    private static var instance : NewServer!;
    var updated = false
    
    private override init() {
        
    }
    
    class func sharedInstance() -> NewServer {
        if (self.instance == nil) {
            self.instance = NewServer();
        }
        return self.instance;
    }
    
    func initializationPush(deviceID: String, firebaseID: String, completionBlock: @escaping (_ success: Bool,_ error: Error?) ->()) {
        
        print("~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("deviceID = \(deviceID)")
        print("firebaseID = \(firebaseID)")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~")
        
        let deviceUUID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        
        let url = "http://api.classatcofc.com/device?device_id=\(deviceUUID)&firebase_id=\(firebaseID)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (response) in
            
            print(response)
            
            if let error = response.error {
                completionBlock(false, error)
            } else {
                completionBlock(true, nil)
            }
        }
    }
    
    func getTimeLastUpdate(completionBlock: @escaping (_ success: Bool, _ time: Int?, _ error: String?) ->()) {
        
        let url = "http://api.classatcofc.com/timestamp"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<TimeLastUpdateResponse>) in
            
            let timeLastUpdateResponse = response.result.value
            
            completionBlock(true, timeLastUpdateResponse?.time, nil)
        }
    }
    
    func getAllCategoriesAndStickers(completionBlock: @escaping (_ success: Bool, _ error: String?) ->()) {
        
        let url = "http://api.classatcofc.com/stickers"
        
        self.printRequest(url: url)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
            
            let timeLastUpdateResponse = response.result.value
            
            DispatchQueue.global(qos: .utility).async {
                if let categories = timeLastUpdateResponse?.categories {
                    var i = 0
                    for category in categories {
                        category.downloadImage(completion: { (success) in
                            if success {
                                let userDefaults: UserDefaults = UserDefaults(suiteName: "group.com.testKey")!
                                print("success save categoryId = \(category.id)")
                                userDefaults.set(category.id, forKey: "categoryId\(i)")
                                userDefaults.set(category.name, forKey: "categoryName\(i)")
                                userDefaults.set(category.timestamp, forKey: "categoryTimestamp\(i)")
                                userDefaults.set(category.isHide, forKey: "categoryIsHide\(i)")
                                userDefaults.set(category.iconData!, forKey: "categoryIconData\(i)")
                                i += 1
                                userDefaults.set(i, forKey: "categoryCount")
                                userDefaults.synchronize()
                            } else {
                                print("Error")
                            }
                        })
                    }
                }
                
                if let stickers = timeLastUpdateResponse?.stickers {
                    var i = 0
                    for sticker in stickers {
                        let type = sticker.iconUrl.components(separatedBy: ".").last!
                        if type == "gif" {
                            let userDefaults: UserDefaults = UserDefaults(suiteName: "group.com.testKey")!
                            print("success save stickerId = \(sticker.id)")
                            userDefaults.set(sticker.id, forKey: "stickerId\(i)")
                            userDefaults.set(sticker.categoryId, forKey: "stickerCategoryId\(i)")
                            userDefaults.set(sticker.serialNumber, forKey: "stickerSerialNumber\(i)")
                            userDefaults.set(sticker.timestamp, forKey: "stickerTimestamp\(i)")
                            userDefaults.set(sticker.isHide, forKey: "stickerIsHide\(i)")
                            userDefaults.set(type, forKey: "stickerType\(i)")
                            userDefaults.set(sticker.iconUrl, forKey: "stickerIconUrl\(i)")
                            i += 1
                            userDefaults.set(i, forKey: "stickersCount")
                            userDefaults.synchronize()
                            if i == stickers.count {
                                
                            }
                        } else {
                            sticker.downloadImage(completion: { (success) in
                                if success {
                                    let userDefaults: UserDefaults = UserDefaults(suiteName: "group.com.testKey")!
                                    print("success save stickerId = \(sticker.id)")
                                    userDefaults.set(sticker.id, forKey: "stickerId\(i)")
                                    userDefaults.set(sticker.categoryId, forKey: "stickerCategoryId\(i)")
                                    userDefaults.set(sticker.serialNumber, forKey: "stickerSerialNumber\(i)")
                                    userDefaults.set(sticker.timestamp, forKey: "stickerTimestamp\(i)")
                                    userDefaults.set(sticker.isHide, forKey: "stickerIsHide\(i)")
                                    userDefaults.set(type, forKey: "stickerType\(i)")
                                    userDefaults.set(sticker.iconData!, forKey: "stickerIconData\(i)")
                                    userDefaults.set(sticker.iconUrl, forKey: "stickerIconUrl\(i)")
                                    i += 1
                                    userDefaults.set(i, forKey: "stickersCount")
                                    userDefaults.synchronize()
                                } else {
                                    print("Error")
                                }
                            })
                        }
                    }
                }
                DispatchQueue.main.async {
                    completionBlock(true, nil)
                }
            }
        }
    }
    
    func getLastCategoriesAndStickers(time: Int, completionBlock: @escaping (_ success: Bool, _ error: String?) ->()) {
        
        let url = "http://api.classatcofc.com/stickers/time?time=\(time)"
        
        self.printRequest(url: url)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
            
            let timeLastUpdateResponse = response.result.value
            
            DispatchQueue.global(qos: .utility).async {
                
                if let categories = timeLastUpdateResponse?.categories {
                    let userDefaults: UserDefaults = UserDefaults(suiteName: "group.com.cofcmoji")!
                    userDefaults.set(categories.count, forKey: "categoryCount")
                    var i = 0
                    for category in categories {
                        if !category.isHide {
                            category.downloadImage(completion: { (success) in
                                if success {
                                    userDefaults.set(category.id, forKey: "categoryId\(i)")
                                    userDefaults.set(category.name, forKey: "categoryName\(i)")
                                    userDefaults.set(category.timestamp, forKey: "categoryTimestamp\(i)")
                                    userDefaults.set(category.isHide, forKey: "categoryIsHide\(i)")
                                    userDefaults.set(category.iconData!, forKey: "categoryIconData\(i)")
                                    i += 1
                                } else {
                                    print("Error")
                                }
                            })
                        }
                    }
                    userDefaults.synchronize()
                }
                
                if let stickers = timeLastUpdateResponse?.stickers {
                    let userDefaults: UserDefaults = UserDefaults(suiteName: "group.com.cofcmoji")!
                    userDefaults.set(stickers.count, forKey: "stickersCount")
                    var i = 0
                    for sticker in stickers {
                        if !sticker.isHide {
                            let type = sticker.iconUrl.components(separatedBy: ".").last!
                            print(type)
                            sticker.downloadImage(completion: { (success) in
                                if success {
                                    userDefaults.set(sticker.id, forKey: "stickerId\(i)")
                                    userDefaults.set(sticker.categoryId, forKey: "stickerCategoryId\(i)")
                                    userDefaults.set(sticker.serialNumber, forKey: "stickerSerialNumber\(i)")
                                    userDefaults.set(sticker.timestamp, forKey: "stickerTimestamp\(i)")
                                    userDefaults.set(sticker.isHide, forKey: "stickerIsHide\(i)")
                                    userDefaults.set(sticker.iconData!, forKey: "stickerIconData\(i)")
                                    i += 1
                                } else {
                                    print("Error")
                                }
                            })
                        }
                    }
                    userDefaults.synchronize()
                }
                
                DispatchQueue.main.async {
                    completionBlock(true, nil)
                }
            }
        }
    }
    
    func printRequest(url: String) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            let json = JSON(data: response.data!, options: .mutableContainers, error: nil)
            print(json)
        }
    }
}
