//
//  TutorialVC.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 26.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import UIKit

class TutorialVC: UIViewController, UIScrollViewDelegate {
    
    private let Width = UIScreen.main.bounds.size.width
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.delegate = self
        
        self.scrollView.contentSize = CGSize(width: 3 * Width, height: self.scrollView.frame.height)
    }
    
    // MARK: - ScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = self.scrollView.frame.width
        let currentPage: CGFloat = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        self.pageControl.currentPage = Int(currentPage)
    }
    
    @IBAction func btBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
