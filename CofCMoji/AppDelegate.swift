//
//  AppDelegate.swift
//  CofCMoji
//
//  Created by Evgeniy Tkachenko on 05.01.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        
//        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
//            
//            
//            let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
//            
//            if let aps = userInfo?["aps"] as? NSDictionary {
//                if let alert = aps["alert"] as? NSDictionary {
//                    if let typeOfPush = alert["typeOfPush"] as? String {
//                        if typeOfPush == "1" {
//                            // OPEN SHOP
//                            
//                            UserDefaults.standard.set("open", forKey: "aps")
//                            UserDefaults.standard.synchronize()
//                        }
//                    }
//                } else if let alert = aps["alert"] as? NSString {
//                    print(alert)
//                }
//            }
//        }
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Fabric.with([Crashlytics.self])
        
        return true
    }
    
    func tokenRefreshNotification(deviseID: String) {
        NewServer.sharedInstance().initializationPush(deviceID: "ios", firebaseID: deviseID) { (success, error) in
            if success {
                print("GO")
            } else {
                print(error!.localizedDescription)
            }
        }
    }
   
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let typeOfPush = alert["typeOfPush"] as? String {
                    if typeOfPush == "1" {
                        // OPEN SHOP
                        let navController = window?.rootViewController as? UINavigationController
                        if navController?.viewControllers.count == 1 {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = storyboard.instantiateViewController(withIdentifier :"WebVC") 
                            navController?.pushViewController(viewController, animated: true)
                        }
                    }
                }
            } else if let alert = aps["alert"] as? NSString {
                print(alert)
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print(token)

        self.tokenRefreshNotification(deviseID: token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
}

