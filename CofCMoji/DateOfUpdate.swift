//
//  DateOfUpdate.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 04.01.17.
//  Copyright © 2017 BigDig. All rights reserved.
//

import RealmSwift

class DateOfUpdate: Object {
    dynamic var id = 0
    dynamic var dateOfUpdate = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
